import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";

import {UsersService} from "../../shared/services/users.service";
import {Message} from "../../shared/models/message.model";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  message: Message;

  constructor(
      private usersServise: UsersService,
      private authSevice: AuthService,
      private router: Router,
      private route: ActivatedRoute
  ) { }

  ngOnInit(){

    this.message = new Message('danger', '');

    this.route.queryParams.subscribe((params: Params)=>{

      if(params['nowCanLogin']){
        this.showMessage('Теперь можете зайти в стстему', 'success');
      }

    });

    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
  }

  private showMessage(text: string, type: string = 'danger'){
    this.message = new Message(type, text);
    setTimeout(()=>{
      this.message.text = '';
    }, 4000)
  }

  onSubmit(){
    const formData = this.form.value;

    this.usersServise.getUserByEmail(formData.email).subscribe(users => {
      if(users[0]){
        if(users[0].password === formData.password){
          this.message.text = '';
          localStorage.setItem('user', JSON.stringify(users[0]));
          this.router.navigate(['/system', 'bill']);
        }else{
          this.showMessage('Пароль не верный');
        }
      }else{
        this.showMessage('Пользователя не существует!');
      }
    })
  }

}
