import {Injectable} from "@angular/core";

@Injectable()
export class AuthService{

    private isAuthenticated: boolean = false;

    login(){
        this.isAuthenticated = true;
    }

    logout(){
        this.isAuthenticated = false;
        localStorage.clear();
    }

    isLoggetIn(): boolean{
        return this.isAuthenticated;
    }
}