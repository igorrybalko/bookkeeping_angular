import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
//import 'rxjs/add/operator/map';

import {User} from "../models/user.model";

@Injectable()
export class UsersService {

  constructor(private http: HttpClient) { }

  getUserByEmail(email: string): Observable<User[]>{
    return this.http.get<User[]>(`http://localhost:3000/users?email=${email}`);
  }

  createNewUser(user: User): Observable<User>{
    return this.http.post<User>('http://localhost:3000/users', user);
  }
}
