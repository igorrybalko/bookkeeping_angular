import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Bill} from "../models/bill.model";
import {BaseApi} from '../core/base-api';

@Injectable()
export class BillService extends BaseApi{
    constructor(public http: HttpClient){
        super(http);
    }

  getBill(): Observable<Bill>{
    return this.get('bill');
  }

  getCurrency(): Observable<any>{
    return this.get('rate');
  }

  updateBill(bill: Bill): Observable<Bill>{
      return this.put('bill', bill);
    }
}