import {Component, OnDestroy, OnInit} from '@angular/core';
import {BillService} from "../shared/services/bill.service";
import {Observable} from "rxjs/Rx";
import {Bill} from "../shared/models/bill.model";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-bill-page',
  templateUrl: './bill-page.component.html',
  styles: []
})
export class BillPageComponent implements OnInit, OnDestroy {

  sub1: Subscription;
  sub2: Subscription;

  currency: any;
  bill: Bill;
  isloaded: boolean = false;

  constructor(
      private billService: BillService
  ) { }

  ngOnInit() {

    this.sub1 = Observable.combineLatest(
        this.billService.getBill(),
        this.billService.getCurrency()
    ).subscribe((data: [Bill, any]) =>{
        this.bill = data[0];
        this.currency = data[1];
        this.isloaded = true;
    });

  }

  ngOnDestroy(){
    this.sub1.unsubscribe();
    if(this.sub2) this.sub2.unsubscribe();
  }

  onRefresh(){
    this.isloaded = false;
    this.sub2 = this.billService.getCurrency().subscribe((currency: any)=>{
      this.currency = currency;
      this.isloaded = true;
    });
  }

}
