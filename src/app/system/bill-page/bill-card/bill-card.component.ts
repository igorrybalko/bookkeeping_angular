import {Component, Input, OnInit} from '@angular/core';
import {Bill} from "../../shared/models/bill.model";

@Component({
  selector: 'app-bill-card',
  templateUrl: './bill-card.component.html'
})
export class BillCardComponent implements OnInit {

  @Input() bill: Bill;
  @Input() currency: any;

  dollar: number;
  rubl: number;
  pound: number;

  ngOnInit() {

    const {rates} = this.currency;

    this.dollar = rates['USD'] * this.bill.value;
    this.rubl = rates['RUB'] * this.bill.value;

  }

}
