import { Component, OnInit } from '@angular/core';
import {Category} from "../shared/models/category.model";
import {CategoriesService} from '../shared/services/categories.service';

@Component({
  selector: 'app-records-page',
  templateUrl: './records-page.component.html'
})
export class RecordsPageComponent implements OnInit {

  categories: Category[] = [];
  isLoaded: boolean = false;

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {

    this.categoriesService.getCategories().subscribe((categories: Category[])=>{
      this.categories = categories;
      this.isLoaded = true;
    })
  }

  newCategoryAdded(category: Category){

    this.categories.push(category);

  }

  categoryWasEdited(category: Category){

    const ind = this.categories.findIndex( c => c.id === category.id );
    this.categories[ind] = category;

  }

}
